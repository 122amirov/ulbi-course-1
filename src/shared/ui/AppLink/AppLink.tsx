import React from 'react'
import { Link, LinkProps } from 'react-router-dom'
import { classNames } from 'shared/lib/classNames'
import cls from './AppLink.module.scss'
import { AppLinkTheme } from 'shared/ui/AppLink/AppLink.model'

interface Props extends LinkProps {
    theme: AppLinkTheme
}

const AppLink: React.FC<Props> = (props) => {
    const { children, theme } = props
    return (
        <Link className={classNames(cls[theme], cls.link)} {...props}>
            {children}
        </Link>
    )
}

export default AppLink
