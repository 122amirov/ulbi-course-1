import React, { ButtonHTMLAttributes } from 'react'
import { classNames } from 'shared/lib/classNames'
import cls from './Button.module.scss'
import { ButtonTheme } from 'shared/ui/Button/Button.model'

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string
    theme: ButtonTheme
}

const Button: React.FC<Props> = (props) => {
    const { className, theme, children, ...otherprops } = props
    return (
        <button className={classNames(cls[theme], cls.button, className)} {...otherprops}>
            {children}
        </button>
    )
}

export default Button
