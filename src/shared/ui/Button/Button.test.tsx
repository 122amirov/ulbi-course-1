import Button from './Button'
import { ButtonTheme } from './Button.model'
import { render, screen } from '@testing-library/react'

describe('Button test', () => {
    test('test 1', () => {
        render(<Button theme={ButtonTheme.clear}>Test</Button>)
        expect(screen.getByText('Test')).toBeInTheDocument()
    })
})
