type Mode = Record<string, boolean>

export function classNames (...res: Array<Mode | string | string[]>): string {
    return res.reduce<string[]>(
        (prev, currentValue, _, array) => {
            if (Array.isArray(currentValue)) {
                return [...prev, ...currentValue]
            } else if (typeof currentValue === 'string') {
                return [...prev, currentValue]
            } else if (!currentValue) {
                return [...prev]
            }
            return [
                ...prev,
                ...Object
                    .entries(currentValue)
                    .filter(([key, value]) => value)
                    .map(([key, val]) => key)
            ]
        }, []).join(' ')
}
