import { classNames } from 'shared/lib/classNames'

describe('classNames function', () => {
    test('classNames последовательный тест', () => {
        expect(classNames('Test1 Test2', 'Test3', 'Test4')).toBe('Test1 Test2 Test3 Test4')
    })
})
