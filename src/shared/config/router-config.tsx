import { RouteProps } from 'react-router-dom'
import { MainPage } from 'pages/main'
import { AboutPage } from 'pages/about'
import { NotFound } from 'pages/not-found'

export const routerConfig: RouteProps[] = [
    { path: '/', element: <MainPage/> },
    { path: '/about', element: <AboutPage/> },
    { path: '*', element: <NotFound/> }
]
