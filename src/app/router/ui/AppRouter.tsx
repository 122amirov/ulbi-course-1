import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { routerConfig } from 'shared/config/router-config'
import { PageLoader } from 'widgets/PageLoader/ui/PageLoader'

export const AppRouter: React.FC = () => {
    return (
        <div className="content-page__page">
            <React.Suspense fallback={<PageLoader/>}>
                <Routes>
                    {routerConfig.map(item => <Route key={item.path} {...item}/>)}
                </Routes>
            </React.Suspense>
        </div>
    )
}
