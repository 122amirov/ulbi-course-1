import React, { useEffect } from 'react'
import { useTheme } from 'app/providers/ThemeProvider'
import { AppRouter } from 'app/router'
import NavBar from 'widgets/Navbar/ui/NavBar'
import { Sidebar } from 'widgets/Sidebar'

const App: React.FC = () => {
    const { theme } = useTheme()
    return (
        <React.Suspense fallback={'Загрузка...'}>
            <div className={`app ${theme}`}>
                <NavBar/>
                <div className="container">
                    <div className="content-page">
                        <Sidebar/>
                        <AppRouter/>
                    </div>
                </div>
            </div>
        </React.Suspense>
    )
}

export default App
