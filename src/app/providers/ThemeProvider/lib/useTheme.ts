import { useContext } from 'react'
import { LOCAL_STORAGE_THEME_KEY, Theme, ThemeContext } from './ThemeContext'

export const useTheme = () => {
    const { theme, setTheme } = useContext(ThemeContext)
    const toggleTheme = (): void => {
        const value = theme === Theme.LIGHT ? Theme.DARK : Theme.LIGHT
        setTheme(value)
        localStorage.setItem(LOCAL_STORAGE_THEME_KEY, value)
    }
    return {
        theme,
        toggleTheme
    }
}
