import React from 'react'
import { classNames } from 'shared/lib/classNames'
import cls from './Navbar.module.scss'
import AppLink from 'shared/ui/AppLink/AppLink'
import { AppLinkTheme } from 'shared/ui/AppLink/AppLink.model'
import { useTranslation } from 'react-i18next'
import { LanguageSwitcher } from 'widgets/LanguageSwitcher/LanguageSwitcher'

interface Props {
    className?: string
}

const NavBar: React.FC<Props> = ({ className }) => {
    const { t } = useTranslation()
    return (
        <nav className={classNames(className, cls.navbar)}>
            <div className={classNames(cls.inner, 'container')}>
                <div className={cls.links}>
                    <AppLink theme={AppLinkTheme.primary} to={'/'}>{t('Main')}</AppLink>
                    <AppLink theme={AppLinkTheme.primary} to={'/about'}>{t('About')}</AppLink>
                </div>
                <LanguageSwitcher/>
            </div>
        </nav>
    )
}

export default NavBar
