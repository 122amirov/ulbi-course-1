import React from 'react'
import Light from 'shared/assets/icons/light.svg'
import Dark from 'shared/assets/icons/dark.svg'
import cls from './ThemeSwither.module.scss'
import { Theme, useTheme } from 'app/providers/ThemeProvider'
import Button from 'shared/ui/Button/Button'
import { ButtonTheme } from 'shared/ui/Button/Button.model'
import {useTranslation} from "react-i18next";

const ThemeSwitcher: React.FC = () => {
    const { theme, toggleTheme } = useTheme()
    const { t } = useTranslation()
    return (
        <Button theme={ButtonTheme.clear} onClick={toggleTheme}>
            {theme === Theme.LIGHT
                ? <div className={cls.text}><Dark fill={'#38454f'} width={20} height={20}/>{t('dark')} </div>
                : <div className={cls.text}><Light fill={'white'} width={20} height={20}/>{t('light')} </div>
            }
        </Button>
    )
}

export default ThemeSwitcher
