import React from 'react'
import { useTranslation } from 'react-i18next'
import Button from 'shared/ui/Button/Button'
import { ButtonTheme } from 'shared/ui/Button/Button.model'
import { Language } from 'shared/config/i18n/i18n.model'

interface IProps {
    className?: string
}

export const LanguageSwitcher: React.FC<IProps> = (props) => {
    const { className } = props
    const { t, i18n } = useTranslation()
    const changeLang = (): void => {
        void i18n.changeLanguage(i18n.language === Language.ru ? Language.en : Language.ru)
    }
    return (
        <Button className={className}
            onClick={changeLang}
            theme={ButtonTheme.clear}
        >
            {i18n.language === Language.ru ? t('English') : t('Russian')}
        </Button>
    )
}
