import React from 'react'
import { useTranslation } from 'react-i18next'
import cls from './PageError.module.scss'
import Button from 'shared/ui/Button/Button'
import { ButtonTheme } from 'shared/ui/Button/Button.model'

const PageError = () => {
    const { t } = useTranslation()
    const reloadPage = () => {
        location.reload()
    }

    return (
        <div className={cls.PageError}>
            {t('somethingWentWrong')}
            <Button onClick={reloadPage} theme={ButtonTheme.clear}>
                {t('reloadPage')}
            </Button>
        </div>
    )
}

export default PageError
