import {fireEvent, screen} from "@testing-library/react";
import {Sidebar} from "./Sidebar";
import renderWithTranslation from "shared/tests/renderWithTranslation";

describe("Sidebar",()=>{
    test("test 1",()=>{
        renderWithTranslation(<Sidebar/>)
        const sidebar = screen.getByTestId("sidebar")
        expect(sidebar).toBeInTheDocument()
    })
    test("toggle button",()=>{
        renderWithTranslation(<Sidebar/>)
        const sidebar = screen.getByTestId("sidebar")
        const toggleButton = screen.getByTestId("sidebar-toggle-btn")
        fireEvent.click(toggleButton)
        expect(sidebar).toHaveClass("collapsed")
        console.log(sidebar.classList.keys())
        fireEvent.click(toggleButton)
    })
})
