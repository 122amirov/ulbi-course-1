import React, {useState} from 'react'
import ThemeSwitcher from 'widgets/ThemeSwitcher/ThemeSwitcher'
import cls from './Sidebar.module.scss'
import {classNames} from 'shared/lib/classNames'
import Button from 'shared/ui/Button/Button'
import {ButtonTheme} from 'shared/ui/Button/Button.model'
import {useTranslation} from 'react-i18next'

export const Sidebar: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false)
    const toggleCollapse = (): void => {
        setCollapsed(prevState => !prevState)
    }
    const {t} = useTranslation()
    return (
        <div data-testid={"sidebar"} className={classNames(cls.sidebar, {[cls.collapsed]: collapsed})}>
            <div className={classNames(cls.switchers)}>
                <ThemeSwitcher/>
                <Button
                    data-testid={"sidebar-toggle-btn"}
                    onClick={toggleCollapse}
                    theme={ButtonTheme.clear}>
                    {collapsed ? `>> ${t("Show")}` : `<< ${t('Hide')}`}
                </Button>
            </div>
        </div>
    )
}
