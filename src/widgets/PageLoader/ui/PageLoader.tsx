import React from 'react'
import css from './PageLoader.module.scss'
import Loader from 'shared/ui/Loader/Loader'
export const PageLoader = () => {
    return (
        <div className={css.PageLoader}>
            <Loader/>
        </div>
    )
}
