import React from 'react'
import css from './NotFound.module.scss'
export const NotFound = () => {
    return (
        <div className={css.NotFound}>
            404 - Страница не найдена
        </div>
    )
}
