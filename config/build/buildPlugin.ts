import webpack from 'webpack'
import HTMLWebpack from 'html-webpack-plugin'
import { BuildOptions } from './types/config'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin'

export function buildPlugin ({ paths, isDev }: BuildOptions): webpack.WebpackPluginInstance[] {
    return [
        new MiniCssExtractPlugin(),
        new HTMLWebpack({
            template: paths.html
        }),
        new webpack.ProgressPlugin(),
        new webpack.DefinePlugin({
            __IS_DEV__: isDev
        }),
        isDev && new webpack.HotModuleReplacementPlugin(),
        isDev && new ReactRefreshWebpackPlugin(),
        new BundleAnalyzerPlugin()
    ]
}
